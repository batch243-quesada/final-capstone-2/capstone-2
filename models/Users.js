const mongoose = require('mongoose');

const userSchema = new mongoose.Schema(
		{
			firstName : {
				type : String,
				required : [true, "First Name is required."]
			},
			lastName : {
				type : String,
				required : [true, "Last Name is required."]
			},
			username: {
				type : String,
				required : [true, "Username is required."],
				unique: true
			},
			email : {
				type : String,
				required : [true, "Email is required."],
				unique: true
			},
			password : {
				type : String,
				required : [true, "Password is required."]
			},
			mobileNo : {
				type : String, 
				required : [true, "Mobile No is required."]
			},
			isAdmin : {
				type : Boolean,
				default : false
			}
		},
		{
			timestamps: true
		}
	)

module.exports = mongoose.model("User", userSchema);
