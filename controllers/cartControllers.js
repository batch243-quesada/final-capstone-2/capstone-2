// import modules
const Product = require('../models/Products');
const User = require('../models/Users');
const Cart = require('../models/Cart');
const Order = require('../models/Orders');
const	authen = require('../authen');

// Add to cart
const addToCart = (request, response) => {
	// get User payload
	const userData = authen.decode(request.headers.authorization);

	// get target Product details
	const productId = request.params.productId;
	return Product.findById(productId)
	.then(result => {

		if(!userData.isAdmin) {
			if(result.isActive) {

				let newCart = new Cart(
						{
							userId: userData.id,
							productId,
							quantity: request.body.quantity
						}
					)

				return newCart.save()
				.then(cart => {
					console.log(cart);
					response.send(`
							ADDED TO CART
							Item: ${cart.productId}
							Title: ${result.title}
							Quantity: ${cart.quantity} pcs
						`)	
				}).catch(error => {
					console.log(error);
					response.send(`Sorry, an error has occured. Please try again.`);
				})
			} else response.send(`Item will be available soon.`);
		} else response.send(`Users with Admin roles are not allowed to make purhcases. You must login as a Regular User in order to do so. Thank you.`)
	
	}).catch(error => {
		console.log(error);
		response.send(`Sorry, an error has occured. Please try again.`);
	})
}

module.exports = {
	addToCart
}