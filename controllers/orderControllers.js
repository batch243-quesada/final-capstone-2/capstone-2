// import modules
const Product = require('../models/Products');
const User = require('../models/Users');
const Order = require('../models/Orders');
const	authen = require('../authen');

// Order a product
const order = (request, response) => {
	// get User payload
	const userData = authen.decode(request.headers.authorization);

	// get target Product details
	const productId = request.params.productId;
	const toBeOrdered = Product.findById(productId)
	.then(result => {

		if(!userData.isAdmin) {
			if(result.isActive) {
				let newOrder = new Order(
						{
							userId: userData.id,
							productId,
							quantity: request.body.quantity
							amount: result.price * quantity
						}
					)

				
			} else response.send(`Item will be available soon.`);
		} else response.send(`Users with Admin roles are not allowed to make purhcases. You must login as a Regular User in order to do so. Thank you.`)
	}).catch(error => {
		console.log(error);
		response.send(`Sorry, an error has occured. Please try again.`);
	})
}

	// const userData = authen.decode(request.headers.authorization)

	// if(!userData.isAdmin) {
	// 	let productId = request.params.productId;

	// 	let orderMade = Product.findById(productId)
	// 	.then(result => {

	// 		if(result.isActive) {
	// 			let newOrder = new Order(
	// 					{
	// 						userId: userData.id,
	// 						productId: productId,
	// 						amount: result.price
	// 					}
	// 				)

	// 			return newOrder
	// 			.save()
	// 			.then(result => {
	// 				console.log(result);
	// 				response.send(`An order has been made.`)
	// 			})
	// 			.catch(error => {
	// 				console.log(error);
	// 				response.send(`Sorry, an error has occured. Please try again.`)
	// 			})
	// 		} else response.send(`Item will be available soon.`) 
	// 	})
	// 	.catch(error => {
	// 		console.log(error);
	// 		response.send(`Sorry, an error has occured. Please try again.`)
	// 	})
	// } else response.send(`Admin roles are not allowed to create an order.`)
// }

module.exports = {
	order
}