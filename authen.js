// dependencies
const jwt = require('jsonwebtoken');

const secret = "ECommerceAPI"

// token
const createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {})
}

// verification
const verify = (request, response, next) => {
	let token = request.headers.authorization;

	if (token !== undefined) {
		token = token.slice(7, token.length);
		// console.log(token);

		return jwt.verify(token, secret, (error) => {
			if (error) response.send(`Invalid token!`);
			else next();
		})
	}
	else response.send(`Authentication failed! No token provided.`);
}

// decrypt
const decode = (token) => {
	if (token === undefined) null;

	else {
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (error) => {

			if (error) {
				return null
			}
			else {
				return jwt.decode(token, {complete: true}).payload
			}
		})
	}
}

module.exports = {
	createAccessToken,
	verify,
	decode
}